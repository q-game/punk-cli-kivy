#!../env/bin/python
# encoding: utf-8
from cli_settings import ADRS

import zmq
import json
import time
from pprint import pprint

import os
#os.environ['KIVY_AUDIO'] = 'pygame'
from kivy.app import App
from kivy.lang import Builder
from kivy.core.window import Window
from kivy.core.text import LabelBase
from kivy.clock import Clock
from kivy.factory import Factory
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.modalview import ModalView
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ListProperty, ObjectProperty, StringProperty
from kivy.core.audio import SoundLoader

from kivy.utils import get_color_from_hex as c

from gui.styles import colors


zmq_ctx = zmq.Context()

class ControlBtn(Button):
    descr = StringProperty()
    ok_text = StringProperty()

class ControlLogEntryBtn(ControlBtn):
    control_method = StringProperty()

class ControlsLogEntry(BoxLayout):
    pass


class EngineCli(object):
    def __init__(self, app, **kwargs):
        self.app = app

    def connect(self):
        self.app.log(u"Ждем пока ответит движок.")
        self.sock = zmq_ctx.socket(zmq.REQ)
        self.sock.setsockopt(zmq.LINGER, 1000)
        #self.sock.setsockopt(zmq.RCVTIMEO, 1000)
        #self.sock.setsockopt(zmq.SNDTIMEO, 1000)
        self.sock.setsockopt(zmq.REQ_RELAXED, 1)
        self.sock.setsockopt(zmq.REQ_CORRELATE, 1)

        address = "tcp://{ip}:{port}".format(**ADRS['CLI_SRV'])
        self.sock.connect(address)
        self.wait_connect_clbk = lambda dt: self.wait_connect()
        Clock.schedule_interval(self.wait_connect_clbk, 0.5)

    def wait_connect(self):
        try:
            self.sock.send_string(json.dumps({'type': 'cmd', 'cmd': 'system::ping'}), zmq.NOBLOCK)
            resp = json.loads(self.sock.recv_string())
        except zmq.Again as e:
            return

        Clock.unschedule(self.wait_connect_clbk)
        self.app.log(u"Движок ответил. Можно переходить к инициализации команты(кнопка 'Проверка')")
        self.app.root.get_screen('quest').ids.btn_start_room.disabled = False

        self.sub_sock = zmq_ctx.socket(zmq.SUB)
        self.sub_sock.setsockopt(zmq.SUBSCRIBE, "")
        address = "tcp://{ip}:{port}".format(**ADRS['CLI_PUB'])
        self.sub_sock.connect(address)

        Clock.schedule_interval(self.get_pub_message, 0.1)


    def get_pub_message(self, *args):
        try:
            request = json.loads(self.sub_sock.recv_string(zmq.NOBLOCK))
        except zmq.Again as e:
            return

        if request.get('type', None) == 'log':
            self.app.log(request.get('data'), request.get('log_level'))
        elif request.get('type', None) == 'event':
            self.app.event(request.get('data'))
        elif request.get('type', None) == 'height':
            self.app.height(request.get('data'))
        elif request.get('type', None) == 'controls':
            self.app.controls(
                request.get('descr'),
                request.get('controls'),
            )
        elif request.get('type', None) == 'manual_controls':
            self.app.manual_controls(
                request.get('descr'),
                request.get('controls'),
            )
        elif request.get('type', None) == 'step_event':
            self.app.step_event(request.get('data'))
        elif request.get('type', None) == 'sound':
            self.app.sound(
                request.get('sound'),
                request.get('stop'),
                request.get('repeat'),
                request.get('echo'),
                request.get('volume'),
            )

    def disconnect(self):
        if hasattr(self, 'sock'):
            self.sock.setsockopt(zmq.LINGER, 0)
            self.sock.close()
            del self.sock

    def next_step(self):
        self._request({'type': 'cmd', 'cmd': 'game::next_step'})

    def repeat_step(self):
        self._request({'type': 'cmd', 'cmd': 'game::repeat_step'})

    def open_all(self):
        self._request({'type': 'cmd', 'cmd': 'game::open_all'})

    def emerge_stop(self):
        self._request({'type': 'cmd', 'cmd': 'game::emerge_stop'})

    def open_close_vhod(self):
        self._request({'type': 'cmd', 'cmd': 'game::open_close_vhod'})

    def control(self, control):
        self._request({'type': 'cmd', 'cmd': 'game::control', 'data':{
            'control': control
        }})

    def prepare_new(self):
        resp = self._request({'type': 'cmd', 'cmd': 'game::prepare_new'})
        return resp.get('result') if resp else None

    def game_start(self):
        resp = self._request({'type': 'cmd', 'cmd': 'game::start'})
        return resp.get('result') if resp else None

    def new_game(self):
        self._request({'type': 'cmd', 'cmd': 'game::stop'})
        time.sleep(1)
        resp = self._request({'type': 'cmd', 'cmd': 'game::init'})
        return resp.get('result') if resp else None

    def stop_game(self):
        self._request({'type': 'cmd', 'cmd': 'game::stop'})
        time.sleep(1)

    def _request(self, msg):
        self.sock.send_string(json.dumps(msg))
        resp = json.loads(self.sock.recv_string())
        return resp

class Stage(Label):
    bckg_color = ListProperty(colors.ACCENT)

    def failed(self):
        self.bckg_color = colors.RED

    def solved(self):
        self.bckg_color = colors.GREEN

    def inprogress(self):
        self.bckg_color = colors.YELLOW

class CliApp(App):
    def on_start(self):
        self.eng = EngineCli(self)
        self.eng.connect()
        self.steps = {}
        self.root.get_screen('quest').ids.steps.bind(minimum_height=self.root.get_screen('quest').ids.steps.setter('height'))
        self.root.get_screen('quest').ids.logs.bind(minimum_height=self.root.get_screen('quest').ids.logs.setter('height'))
        self.root.get_screen('manual').ids.manual_controls.bind(minimum_height=self.root.get_screen('manual').ids.manual_controls.setter('height'))

        self.played_sounds = []

    def stop_game(self):
        self.eng.stop_game()
        self.eng.disconnect()

    def prompt(self, btn):
        view = ModalView()
        box = BoxLayout(orientation='vertical')
        close_btn = Button(text=u'Отмена. (Вернуться к интерфейсу управления)', font_name='Roboto', font_size=30, background_color=colors.GREEN)
        ok_btn = Button(text=btn.ok_text, font_name='Roboto', 
                        font_size=30, background_color=colors.RED)
        descr = Label(
            text=btn.descr,
            font_name='Roboto',
            font_size=30,
            background_color=colors.SECONDARY_TEXT,
        )
        ok_btn.bind(on_press=view.dismiss)
        ok_btn.bind(on_press=btn.clbk)
        box.add_widget(descr)
        box.add_widget(close_btn)
        box.add_widget(ok_btn)
        view.add_widget(box)
        close_btn.bind(on_press=view.dismiss)
        view.open()

    def on_stop(self):
        self.stop_game()

    def qg_new_game(self):
        self.root.get_screen('quest').ids.steps.clear_widgets()
        self.root.get_screen('quest').ids.logs.clear_widgets()
        self.root.get_screen('quest').ids.btn_start_room.disabled = True
        self.steps = {}
        result = self.eng.new_game()
        if result:
            for step in result:
                stage = Stage(text=step['name'])
                self.steps.setdefault(step['id'], stage)
                self.root.get_screen('quest').ids.steps.add_widget(stage)


    def qg_prepare_new(self):
        self.root.get_screen('quest').ids.steps.clear_widgets()
        self.root.get_screen('quest').ids.logs.clear_widgets()
        self.root.get_screen('quest').ids.btn_prepare_game.disabled = True
        self.steps = {}
        result = self.eng.prepare_new()
        if result:
            for step in result:
                stage = Stage(text=step['name'])
                self.steps.setdefault(step['id'], stage)
                self.root.get_screen('quest').ids.steps.add_widget(stage)

    def qg_game_start(self):
        self.root.get_screen('quest').ids.steps.clear_widgets()
        self.root.get_screen('quest').ids.logs.clear_widgets()
        self.root.get_screen('quest').ids.btn_start_game.disabled = True
        self.root.get_screen('quest').ids.btn_prepare_game.disabled = True
        self.steps = {}
        result = self.eng.game_start()
        if result:
            for step in result:
                stage = Stage(text=step['name'])
                self.steps.setdefault(step['id'], stage)
                self.root.get_screen('quest').ids.steps.add_widget(stage)

    def qg_next_step(self):
        self.eng.next_step()

    def qg_repeat_step(self):
        self.eng.repeat_step()

    def qg_open_all(self):
        self.eng.open_all()

    def qg_emerge_stop(self):
        self.eng.emerge_stop()

    def qg_open_close_vhod(self):
        self.eng.open_close_vhod()

    def qg_control(self, control):
        self.eng.control(control)

    def log(self, data, log_level='info'):
        log_text = unicode(data)
        if u'провалился' in log_text:
            log_level = 'error'

        log_entry_level = {
                'error': Factory.ErrorLogEntry,
                'info': Factory.InfoLogEntry,
                'tip': Factory.TipLogEntry,
                'legend': Factory.LegendLogEntry,
            }

        self.root.get_screen('quest').ids.logs.add_widget(log_entry_level[log_level](text=log_text))

    def get_controls_entry(self, descr, controls):
        entry = ControlsLogEntry(orientation='vertical')
        entry.height = 75
        entry.add_widget(Label(text=descr, color=(0,0,0,1), height=30))
        entry_controls = BoxLayout(
            orientation='horizontal',
            height=45,
            size_hint=(1, None),
            padding=1,
            spacing=1,
        )
        entry.add_widget(entry_controls)
        for control in controls:
            entry_controls.add_widget(ControlLogEntryBtn(
                text=control['descr'],
                control_method=control['method'],
                on_press=lambda btn: self.qg_control(btn.control_method),
            ))
        return entry

    def controls(self, descr, controls):
        entry = self.get_controls_entry(descr, controls)
        self.root.get_screen('quest').ids.logs.add_widget(entry)

    def manual_controls(self, descr, controls):
        entry = self.get_controls_entry(descr, controls)
        self.root.get_screen('manual').ids.manual_controls.add_widget(entry)


    def sound(self, sound, stop=False, repeat=False, echo=False, volume=1):
        soundtrack = False
        if sound == 'soundtrack':
            soundtrack = True

        if not echo:
            if sound in self.played_sounds:
                return

            self.played_sounds.append(sound)

        sound = SoundLoader.load('sounds/{0}.wav'.format(sound))
        if soundtrack:
            self.soundtrack = sound
        sound.volume = volume
        if repeat:
            sound.loop = True
        sound.play()


    def event(self, data):
        enable = data.get('enable', None)
        if enable:
            if enable == 'prepare_game':
                self.root.get_screen('quest').ids.btn_start_room.disabled = True
                self.root.get_screen('quest').ids.btn_prepare_game.disabled = False
                self.root.get_screen('quest').ids.btn_start_game.disabled = False
                self.root.get_screen('quest').ids.btn_open_close_vhod.disabled = False
                self.root.get_screen('quest').ids.btn_open_all.disabled = False
                #self.root.get_screen('quest').ids.btn_emerge_stop.disabled = False
                #self.root.get_screen('quest').ids.btn_repeat_step.disabled = False
                self.root.get_screen('quest').ids.btn_next_step.disabled = False
                self.root.get_screen('quest').ids.btn_game_over.disabled = False
                self.root.get_screen('quest').ids.btn_go_to_manual_screen.disabled = False
            if enable == 'start_game':
                self.root.get_screen('quest').ids.btn_start_room.disabled = True
                self.root.get_screen('quest').ids.btn_prepare_game.disabled = True
                self.root.get_screen('quest').ids.btn_start_game.disabled = False
                self.root.get_screen('quest').ids.btn_open_close_vhod.disabled = False
                self.root.get_screen('quest').ids.btn_open_all.disabled = False
                #self.root.get_screen('quest').ids.btn_emerge_stop.disabled = False
                #self.root.get_screen('quest').ids.btn_repeat_step.disabled = False
                self.root.get_screen('quest').ids.btn_next_step.disabled = False
                self.root.get_screen('quest').ids.btn_game_over.disabled = False
                self.root.get_screen('quest').ids.btn_go_to_manual_screen.disabled = False

        temp_disable = data.get('temp_disable', None)
        if temp_disable:
            btn = self.root.get_screen('quest').ids['btn_{0}'.format(temp_disable)]
            btn.disabled = True
            def disable_btn(btn):
                btn.disabled = False
            Clock.schedule_once(lambda dt: disable_btn(btn), 1)

    def height(self, height):
        if height:
            self.root.get_screen('quest').ids.height_label.text = u"H: " + unicode(height)


    def step_event(self, data):
        stage = self.steps.get(int(data['id']), None)
        if stage:
            if data['status'] == 'failed':
                stage.failed()
            elif data['status'] == 'solved':
                stage.solved()
            elif data['status'] == 'inprogress':
                stage.inprogress()


    def build(self):
        return Builder.load_file('./gui/cli.kv')

if __name__ == '__main__':
    Window.clearcolor = colors.LIGHT_PRIMARY
    LabelBase.register(name='Roboto',
                       fn_regular='fonts/roboto/Roboto-Thin.ttf',
                       fn_bold='fonts/roboto/Roboto-Medium.ttf')

    CliApp().run()
