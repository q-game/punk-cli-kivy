import itertools
import png

import colors
from kivy.utils import get_color_from_hex

STYLES = {
    'default': {
        0: [255*item for item in colors.ACCENT_drk],
        1: [255*item for item in colors.ACCENT],
    },
}

ELEMENTS = {
    'btn': """
00000000
01111110
01111110
01111110
01111110
01111110
01111110
00000000
""",
    'btn_down': """
11111111
10000001
10000001
10000001
10000001
10000001
10000001
11111111
"""
}


def render_pixels(tmpl, styles):
    skeleton = [[int(point) for point in list(row)] for row in tmpl.split()]
    pixels = [[styles[point] for point in row] for row in skeleton]
    pixels = [list(itertools.chain(*row)) for row in pixels]
    return pixels

def render_file(pixels, file_name):
    png.from_array(pixels, 'RGBA').save(file_name + '.png')

def restyle():
    for name, style in STYLES.iteritems():
        name = '' if name == 'default' else '_' + name
        for element, tmpl in ELEMENTS.iteritems():
            pixels = render_pixels(tmpl, style)
            render_file(pixels, element + name)

if __name__ == '__main__':
    restyle()


