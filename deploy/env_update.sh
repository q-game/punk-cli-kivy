#!/bin/bash
# Удаляем старое виртуальное окружение, устанавливаем новое, накладываем патчи

cd `dirname "$0"`;
ENV_PATH='../../env/';
ROOT_PATH='../../';

sudo apt-get install libzmq3-dev
sudo apt-get install libglew1.5-dev
sudo apt-get install libsdl2-2.0-0 libsdl2-image-2.0-0 libsdl2-mixer-2.0-0 libsdl2-ttf-2.0-0
sudo apt-get install python-setuptools python-pygame python-opengl python-gst0.10 python-enchant gstreamer0.10-plugins-good python-dev build-essential libgl1-mesa-dev libgles2-mesa-dev python-pip libgstreamer1.0-dev

if [ $1 ]; then
    rm -r "$ENV_PATH";
    virtualenv --system-site-packages --python="python$1" "$ENV_PATH";
    #$ENV_PATH/bin/pip install -r env.req;
    $ENV_PATH/bin/pip install cython==0.20
    $ENV_PATH/bin/pip install numpy
    $ENV_PATH/bin/pip install kivy==1.9.0
    $ENV_PATH/bin/pip install pyzmq==14.4.1
    #$ENV_PATHbin/garden install filebrowser 

    #cd $ROOT_PATH
    #mkdir tools
    #cd tools
    #git clone http://github.com/kivy/kivy-designer/

else
   echo "Error: python version requires as first argument" 1>&2;
fi
